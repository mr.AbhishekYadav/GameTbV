//
//  ViewController.swift
//  TotalityCorp_Assingment
//
//  Created by Abhishek Yadav on 09/08/23.
//

import UIKit

class ListingViewController: UIViewController {
    @IBOutlet weak var mTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    func initialSetup() {
        self.setColor()
        self.setText()
        self.mTableView.register(UINib(nibName: GameListTableViewCell.xibName, bundle: nil), forCellReuseIdentifier: GameListTableViewCell.cellIdentifier)
        self.mTableView.dataSource = self
        self.mTableView.delegate = self
        self.mTableView.separatorStyle = .none
    }
    func setColor() {
        self.mTableView.backgroundColor = .white
        self.navigationItem.titleView?.tintColor = .label
    }
    func setText() {
        let titleLabel = UILabel()
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 20)
        ]
        titleLabel.attributedText = NSAttributedString(string: "PLAY", attributes: attributes)
        self.navigationItem.titleView = titleLabel
    }
    
}
extension ListingViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GameListTableViewCell.cellIdentifier, for: indexPath) as! GameListTableViewCell
        cell.innerView.backgroundColor = .yellow
        cell.gameILogo.backgroundColor = .green
        cell.downloadButtonView.backgroundColor = .orange
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
    
}
