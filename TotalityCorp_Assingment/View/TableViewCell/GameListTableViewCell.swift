//
//  GameListTableViewCell.swift
//  TotalityCorp_Assingment
//
//  Created by Abhishek Yadav on 09/08/23.
//

import UIKit

class GameListTableViewCell: UITableViewCell {
    static let cellIdentifier = "GameListTableViewCell"
        static let xibName = "GameListTableViewCell"
    @IBOutlet weak var downloadButtonView: UIView!
    @IBOutlet weak var gameILogo: UIView!
    @IBOutlet weak var innerView: UIView!
    let xibName = "GameListTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        initialConfig()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initialConfig() {
        setColor()
        setCornerRadius()
    }
    func setColor() {
        self.innerView?.backgroundColor = .orange
        self.gameILogo?.backgroundColor = .yellow
        self.downloadButtonView?.backgroundColor = .green
    }
    func setCornerRadius() {
        self.innerView?.layer.cornerRadius = 10.0
        self.gameILogo?.layer.cornerRadius = 10.0
        self.downloadButtonView?.layer.cornerRadius = 10.0
    }
}
